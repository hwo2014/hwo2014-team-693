package noobbot;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;

public class Main {
	public static void main(String... args) throws IOException {
		String host = args[0];
		int port = Integer.parseInt(args[1]);
		String botName = args[2];
		String botKey = args[3];

		System.out.println("Connecting to " + host + ":" + port + " as "
				+ botName + "/" + botKey);

		final Socket socket = new Socket(host, port);
		final PrintWriter writer = new PrintWriter(new OutputStreamWriter(
				socket.getOutputStream(), "utf-8"));
		final BufferedReader reader = new BufferedReader(new InputStreamReader(
				socket.getInputStream(), "utf-8"));

		new Main(reader, writer, new Join(botName, botKey));
	}

	final Gson gson = new Gson();
	private PrintWriter writer;
	private Race race;

	public Main(final BufferedReader reader, final PrintWriter writer,
			final Join join) throws IOException {
		this.writer = writer;
		String line = null;

		send(join);
		Status status = new Status();

		while ((line = reader.readLine()) != null) {
			final MsgWrapper msgFromServer = gson.fromJson(line,
					MsgWrapper.class);

			if (msgFromServer.msgType.equals("carPositions")) {

				status.update(line);

				Throttle throttle = null;

				if (!status.isOnCorner() && !status.isNextPieceOnCorner()) {
					throttle = new Throttle(0.7);
				}

				if (status.isNextPieceOnCorner() && status.throttle == 0.7) {
					throttle = new Throttle(0.4);
				}
				
				if (throttle == null) {
					throttle = new Throttle(0.65);
				}

				send(throttle);

				if (status.updateThrottle(throttle) != 0.0) {
					System.out.println("setting throttle to " + throttle
							+ " at angle " + status.getCarPosition().angle
							+ " at piece " + status.currentPiece
							+ " at piece position " + status.getCarPosition().piecePosition);
				}

			} else if (msgFromServer.msgType.equals("join")) {
				System.out.println(msgFromServer);
			} else if (msgFromServer.msgType.equals("gameInit")) {
				System.out.println(msgFromServer);
				race = gson.fromJson(line, GameInitData.class).data.race;
			} else if (msgFromServer.msgType.equals("gameEnd")) {
				System.out.println(msgFromServer);
			} else if (msgFromServer.msgType.equals("gameStart")) {
				System.out.println(msgFromServer);
			} else {
				System.out.println(msgFromServer + " at " + status);
				send(new Ping());
			}
		}
	}

	private void send(final SendMsg msg) {
		writer.println(msg.toJson());
		writer.flush();
	}

	class Status {
		CarPositions carPositions;
		Piece currentPiece;
		Double throttle;

		public double updateThrottle(Throttle throttle) {
			double diff = this.throttle == null ? throttle.value
					: this.throttle - throttle.value;
			this.throttle = throttle.value;
			return diff;
		}

		public boolean isOnCorner() {
			return isOnCorner(currentPiece);
		}

		public boolean isNextPieceOnCorner() {
			return isOnCorner(getNextPiece());
		}

		public boolean isOnCorner(Piece piece) {
			return piece.angle != null && Math.abs(piece.angle) > 0.0;
		}

		double distanceToNextPiece() {
			return currentPiece.length
					- getCarPosition().piecePosition.inPieceDistance;
		}

		double angleOfNextPiece() {
			return getNextPiece().angle;
		}

		Piece getNextPiece() {
			return race.track.pieces
					.get((getCarPosition().piecePosition.pieceIndex + 1)
							% race.track.pieces.size());
		}

		private void update(String line) {
			carPositions = gson.fromJson(line, CarPositions.class);
			currentPiece = race.track.pieces
					.get(getCarPosition().piecePosition.pieceIndex);
		}

		private CarPosition getCarPosition() {
			return carPositions.data.get(0);
		}

		@Override
		public String toString() {
			return "Status [carPositions=" + carPositions + ", currentPiece="
					+ currentPiece + ", throttle=" + throttle + "]";
		}

	}

}

abstract class SendMsg {
	public String toJson() {
		return new Gson().toJson(new MsgWrapper(this));
	}

	protected Object msgData() {
		return this;
	}

	protected abstract String msgType();
}

class MsgWrapper {
	public final String msgType;
	public final Object data;

	MsgWrapper(final String msgType, final Object data) {
		this.msgType = msgType;
		this.data = data;
	}

	public MsgWrapper(final SendMsg sendMsg) {
		this(sendMsg.msgType(), sendMsg.msgData());
	}

	@Override
	public String toString() {
		return msgType + " : " + data;
	}
}

class CarPosition {
	CarId id;
	double angle;
	PiecePosition piecePosition;
	int lap;

	@Override
	public String toString() {
		return "CarPosition [id=" + id + ", angle=" + angle
				+ ", piecePosition=" + piecePosition + ", lap=" + lap + "]";
	}
}

class PiecePosition {
	int pieceIndex;
	double inPieceDistance;
	Lane lane;

	@Override
	public String toString() {
		return "PiecePosition [pieceIndex=" + pieceIndex + ", inPieceDistance="
				+ inPieceDistance + ", lane=" + lane + "]";
	}
}

class Lane {
	int startLaneIndex;
	int endLaneIndex;
}

class CarPositions {
	List<CarPosition> data;
}

class GameInitData {
	RaceWrapper data;
}

class RaceWrapper {
	Race race;
}

class Race {
	Track track;
	List<Car> cars;
	// String raceSession;
}

class Track {
	String id;
	String name;
	List<Piece> pieces;
	List<LaneDef> lanes;
	StartingPosition startingPosition;
}

class StartingPosition {
	Position position;
	double angle;
}

class Piece {
	public int index;
	double length;
	@SerializedName("switch")
	Boolean isSwitch;
	Double angle;

	boolean isCorner() {
		return angle == 0.0;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((angle == null) ? 0 : angle.hashCode());
		result = prime * result + index;
		result = prime * result
				+ ((isSwitch == null) ? 0 : isSwitch.hashCode());
		long temp;
		temp = Double.doubleToLongBits(length);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Piece other = (Piece) obj;
		if (angle == null) {
			if (other.angle != null)
				return false;
		} else if (!angle.equals(other.angle))
			return false;
		if (index != other.index)
			return false;
		if (isSwitch == null) {
			if (other.isSwitch != null)
				return false;
		} else if (!isSwitch.equals(other.isSwitch))
			return false;
		if (Double.doubleToLongBits(length) != Double
				.doubleToLongBits(other.length))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Piece [length=" + length + ", isSwitch=" + isSwitch
				+ ", angle=" + angle + "]";
	}
}

class Position {
	double x;
	double y;
}

class Car {
	CarId id;
	CarDimensions dimensions;
}

/**
 * @author graham
 *
 */
class CarId {
	String name;
	String color;

	@Override
	public String toString() {
		return "CarId [name=" + name + ", color=" + color + "]";
	}
}

class CarDimensions {
	double length;
	double width;
	double guideFlagPosition;
}

class LaneDef {
	int distanceFromCenter;
	int index;
}

class Join extends SendMsg {
	public final String name;
	public final String key;

	Join(final String name, final String key) {
		this.name = name;
		this.key = key;
	}

	@Override
	protected String msgType() {
		return "join";
	}
}

class Ping extends SendMsg {
	@Override
	protected String msgType() {
		return "ping";
	}
}

class Throttle extends SendMsg {
	double value;

	public Throttle(double value) {
		this.value = value;
	}

	@Override
	protected Object msgData() {
		return value;
	}

	@Override
	protected String msgType() {
		return "throttle";
	}

	@Override
	public String toString() {
		return "Throttle [value=" + value + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		long temp;
		temp = Double.doubleToLongBits(value);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Throttle other = (Throttle) obj;
		if (Double.doubleToLongBits(value) != Double
				.doubleToLongBits(other.value))
			return false;
		return true;
	}

}